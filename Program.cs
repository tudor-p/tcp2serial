﻿//©2013 Tudor Popovici - TCP2Serial "Insistent" Adapter Server
//Created 2013-12-19
//Released under GNU LESSER GENERAL PUBLIC LICENSE v3

//Please note that this code requires .net v3.5 or later

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using TCP2Serial.Properties;

namespace TCP2Serial
{
    class Program
    {
        static string version = "v0.6";
        static string dateUpdated = "2014-06-09";
        static bool keepAlive = true;
        static AutoResetEvent stayAliveAutoResetEvent = new AutoResetEvent(false);
        static byte[] bufToSerial, bufFromSerial;
        /// <summary>
        /// There are a few ols packets that take 4 extra bytes after; this list helps knowing when extra 4 bytes need to be skipped during parsing
        /// </summary>
        static HashSet<byte> ols_protocol_long_command_ids = new HashSet<byte>(new byte[] { 0x00, 0x80, 0xC0, 0xC1, 0xC2, 0x81, 0x82 });
        static int skip = 0;

        static void LogStateMessage(string message)
        {
            if (!Settings.Default.Silent)
            {
                ConsoleColor old = Console.ForegroundColor;
                Console.ResetColor();
                Console.WriteLine(message);
                Console.ForegroundColor = old;
            }
        }
        static void LogErrorMessage(Exception e)
        {
            if (verbose && !Settings.Default.Silent)
            {
                ConsoleColor old = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e.ToString());
                Console.ForegroundColor = old;
            }
        }
        static void LogDataToSerial(int len)
        {
            if (Settings.Default.Spy)
            {
                ConsoleColor old = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.DarkRed;
                if (Settings.Default.Hex)
                {
                    for (int i = 0; i < len; ++i)
                    {
                        Console.Write(" " + bufToSerial[i].ToString(Settings.Default.HexFormat));
                    }
                }
                else
                {
                    Console.Out.Write(ASCIIEncoding.ASCII.GetChars(bufToSerial, 0, len));
                }
                Console.ForegroundColor = old;
            }
        }
        static void LogDataFromSerial(int len)
        {
            if (Settings.Default.Spy)
            {
                ConsoleColor old = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                if (Settings.Default.Hex)
                {
                    for (int i = 0; i < len; ++i)
                    {
                        Console.Write(" " + bufFromSerial[i].ToString(Settings.Default.HexFormat));
                    }
                }
                else
                {
                    Console.Out.Write(ASCIIEncoding.ASCII.GetChars(bufFromSerial, 0, len));
                }
                Console.ForegroundColor = old;
            }
        }
        static void AsyncReceiveFromPC(IAsyncResult o)
        {
            if (!keepAlive) return;
            int bytes_read = 0;
            try
            {
                bytes_read = tcp_client.Client.EndReceive(o);
            }
            catch (Exception e)
            {
                LogErrorMessage(e);
            }
            if (bytes_read <= 0)
            {
                keepAlive = false;
                stayAliveAutoResetEvent.Set();
                LogStateMessage("PC Closed connection.");
                return;
            }
            if (serial_port.IsOpen)
            {
                serial_port.BaseStream.Write(bufToSerial, 0, bytes_read);
                #region OLS Adaptor Behavior
                if (!Settings.Default.Transparent)
                {
                    for (int i = 0; i < bytes_read; ++i)
                    {
                        if (skip > 0)
                        {
                            --skip;
                            continue;
                        }
                        byte ols_packet_type_id = bufToSerial[i];
                        Console.Write("<<" + ols_packet_type_id.ToString("X2"));
                        switch (ols_packet_type_id)
                        {
                            case 0x05:
                                keepAlive = false;
                                LogStateMessage("PC Software RLE Stop Request");
                                stayAliveAutoResetEvent.Set();
                                return;
                        }
                        if (ols_protocol_long_command_ids.Contains(bufToSerial[i]))
                        {
                            skip = 4;
                        }
                    }
                }
                #endregion
                try
                {
                    if (tcp_client.Connected)
                    {
                        tcp_client.Client.BeginReceive(bufToSerial, 0, Settings.Default.InternalBufferLength,
                                SocketFlags.None, AsyncReceiveFromPC, tcp_client);
                        LogDataToSerial(bytes_read);

                    }
                    else
                    {
                        //Console.Write("<");
                        if (keepAlive)
                        {
                            keepAlive = false;
                            stayAliveAutoResetEvent.Set();
                        }
                    }
                }
                catch (Exception e)
                {
                    //Console.Write("<");
                    LogErrorMessage(e);
                    if (keepAlive)
                    {
                        keepAlive = false;
                        stayAliveAutoResetEvent.Set();
                    }
                }
            }
        }
        static void AsyncReceiveFromSerial(IAsyncResult o)
        {
            if (!keepAlive) return;
            int bytes_read = 0;
            try
            {
                bytes_read = serial_port.BaseStream.EndRead(o);
            }
            catch (Exception e)
            {
                LogErrorMessage(e);
                bytes_read = 0;
            }
            if (bytes_read <= 0)
            {
                keepAlive = false;
                stayAliveAutoResetEvent.Set();
                LogStateMessage("Serial Closed Connnection.");
                return;
            }
            if (tcp_client.Client.Connected)
            {
                try
                {
                    tcp_client.Client.Send(bufFromSerial, bytes_read, SocketFlags.None);
                }
                catch (Exception e)
                {
                    LogErrorMessage(e);

                    keepAlive = false;
                    stayAliveAutoResetEvent.Set();
                    LogStateMessage("PC Closed connection.");
                    return;
                }
                serial_port.BaseStream.BeginRead(bufFromSerial, 0, Settings.Default.InternalBufferLength, AsyncReceiveFromSerial, serial_port);
                LogDataFromSerial(bytes_read);
                //Console.Write(">");
            }
        }
        static SerialPort serial_port;
        static TcpListener tcp_listener;
        static TcpClient tcp_client;
        static bool run = true;

        static bool verbose = false;

        static void ShowInfo()
        {
            LogStateMessage("\"Insistent\" TCP Server to Serial Port " + version + " (updated " + dateUpdated + ")");
            LogStateMessage("    (with support for Open Workbench Logic Sniffer v9.7.0)");
            LogStateMessage("(©)2013-2014 Tudor Popovici - GNU LESSER GENERAL PUBLIC LICENSE v3");
            LogStateMessage(" Source code at: https://bitbucket.org/tudor_p/tcp2serial");
            LogStateMessage("   git clone git@bitbucket.org:tudor_p/tcp2serial.git");
            LogStateMessage("");
            LogStateMessage("  [-help|-h|/help|/h|--help] to display help and syntax.");
            LogStateMessage("");
        }
        static void ShowHelp()
        {
            LogStateMessage("Syntax (used to configure persistent settings accross restarts): ");
            LogStateMessage("");
            LogStateMessage("tcp2serial [serialport=<name>] [serialbaud=<value_bps>]");
            LogStateMessage("    [tcpport=<listening_port>] [transparent[=true|=false]]");
            LogStateMessage("    [sump2ols[=true|=false]]");
            LogStateMessage("    [databits=<databits>] [stopbits=<" + StopBits.None + "|" + StopBits.One + "|" + StopBits.OnePointFive + "|" + StopBits.Two + ">]");
            LogStateMessage("    [parity=<" + Parity.None + "|" + Parity.Even + "|" + Parity.Odd + "|" + Parity.Mark + "|" + Parity.Space + ">] [parityreplace=<replacement_byte>]");
            LogStateMessage("    [dtrenable=<true|false>] [rtsenable=<true|false>]");
            LogStateMessage("    [handshake=<" + Handshake.None + "|" + Handshake.RequestToSend + "|" + Handshake.RequestToSendXOnXOff + "|" + Handshake.XOnXOff + ">]");
            LogStateMessage("    [discardnull=<true|false>]");
            LogStateMessage("    [readbuffersize=<buffer_size>] [readtimeout=<milliseconds>]");
            LogStateMessage("    [writebuffersize=<buffer_size>] [writetimout=<milliseconds>]");
            LogStateMessage("    [tcprecievetimeout=<milliseconds>] [tcpsendtimeout=<milliseconds>]");
            LogStateMessage("    [retrytimeout=<milliseconds>] [internalbufferlength=<buffer_size>]");
            LogStateMessage("    [spy[=true|=false]] [hex[=true|false]] [hexformat=[x2|X2]");
            LogStateMessage("    [silent[=true|=false]] [verbose]");
            LogStateMessage("");
            LogStateMessage("  where:");
            LogStateMessage("    <description> needs to be replaced accordingly;");
            LogStateMessage("    [key or value collection] denotes optional config key or value");
            LogStateMessage("    e.g. [=value1|=value2] the value can be configured explicitly or left with default, positive, meaning");
            LogStateMessage("");
            LogStateMessage(" Parameter keys:");
            LogStateMessage("    serialport, serialbaud, tcpport -> essential & self explanatory");
            LogStateMessage("    databits, stopbits, parity, handshake -> extra serial options");
            LogStateMessage("    discardnull, read/write buffer size/timeout -> extra .net options");
            LogStateMessage("    tcp<send/recieve>timeout -> socket async timing configuration");
            LogStateMessage("    retrytimeout -> time for the app to step back after an error");
            LogStateMessage("    internalbufferlength -> internal application send/reieve buffers sizes");
            LogStateMessage("    transparent -> transparent/direct data forwarding (\"cable\" replacement)");
            LogStateMessage("                    (for nontransparent see: sump2ols");
            LogStateMessage("    terminal -> run in terminal mode, no tcp server, redirect port to console");
            LogStateMessage("    silent -> inhibit all state messages");
            LogStateMessage("    spy -> log to console serial<->tcp communications");
            LogStateMessage("    hex -> use hex format for data logging");
            LogStateMessage("    sump2ols -> adapts the UART connection for easy OLS usage");
            LogStateMessage("                  (the opposite of transparent)");
            LogStateMessage("    -h, -help, /help -> only show help and quit");
            LogStateMessage("    verbose -> display detaild exceptions when encountering errors");
            LogStateMessage("");
            LogStateMessage("For the not configured options are considered previous or default values. Malformated options are ignored.");
            LogStateMessage("");
            LogStateMessage(" Example: tcp2serial serialport=COM1 serialbaud=115200 tcpport=8000 transparent");
            LogStateMessage("");
            LogStateMessage("");

        }

        static void ParseParametersToSettings(string[] args)
        {
            foreach (string arg in args)
            {
                string[] kv = arg.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                try
                {
                    switch (kv[0])
                    {
                        case "-h":
                        case "-help":
                        case "--help":
                        case "/h":
                        case "/help":
                            Settings.Default.Silent = false;
                            ShowInfo();
                            ShowHelp();
                            LogStateMessage("Help displayed. Not persisting current command line options. Quiting...");
                            Environment.Exit(0);
                            return;
                        case "verbose":
                            verbose = true;
                            break;
                        case "tcpport":
                            Settings.Default.TCPPort = int.Parse(kv[1]);
                            break;
                        case "serialport":
                            Settings.Default.SerialPortName = kv[1];
                            break;
                        case "serialbaud":
                            Settings.Default.SerialBaud = int.Parse(kv[1]);
                            break;
                        case "databits":
                            Settings.Default.DataBits = int.Parse(kv[1]);
                            break;
                        case "discardnull":
                            Settings.Default.DiscardNull = bool.Parse(kv[1]);
                            break;
                        case "dtrenable":
                            Settings.Default.DtrEnable = bool.Parse(kv[1]);
                            break;
                        case "handshake":
                            Settings.Default.Handshake = (Handshake)Enum.Parse(typeof(Handshake), kv[1]);
                            break;
                        case "parity":
                            Settings.Default.Parity = (Parity)Enum.Parse(typeof(Parity), kv[1]);
                            break;
                        case "parityreplace":
                            Settings.Default.ParityReplace = byte.Parse(kv[1]);
                            break;
                        case "readbuffersize":
                            Settings.Default.ReadBufferSize = int.Parse(kv[1]);
                            break;
                        case "readtimeout":
                            Settings.Default.ReadTimeOut = int.Parse(kv[1]);
                            break;
                        case "writebuffersize":
                            Settings.Default.WriteBufferSize = int.Parse(kv[1]);
                            break;
                        case "rtsenable":
                            Settings.Default.RtsEnable = bool.Parse(kv[1]);
                            break;
                        case "stopbits":
                            Settings.Default.StopBits = (StopBits)Enum.Parse(typeof(StopBits), kv[1]);
                            break;
                        case "writetimout":
                            Settings.Default.WriteTimeOut = int.Parse(kv[1]);
                            break;
                        case "tcprecievetimeout":
                            Settings.Default.TCPRecieveTimeOut = int.Parse(kv[1]);
                            break;
                        case "tcpsendtimeout":
                            Settings.Default.TCPSendTimeOut = int.Parse(kv[1]);
                            break;
                        case "retrytimeout":
                            Settings.Default.RetryTimeOut = int.Parse(kv[1]);
                            break;
                        case "internalbufferlength":
                            Settings.Default.InternalBufferLength = int.Parse(kv[1]);
                            break;
                        case "transparent":
                            switch (kv.Length)
                            {
                                case 2:
                                    Settings.Default.Transparent = bool.Parse(kv[1]);
                                    break;
                                default:
                                    Settings.Default.Transparent = true;
                                    break;
                            }
                            break;
                        case "silent":
                            switch (kv.Length)
                            {
                                case 2:
                                    Settings.Default.Silent = bool.Parse(kv[1]);
                                    break;
                                default:
                                    Settings.Default.Silent = true;
                                    break;
                            }
                            break;
                        case "spy":
                            switch (kv.Length)
                            {
                                case 2:
                                    Settings.Default.Spy = bool.Parse(kv[1]);
                                    break;
                                default:
                                    Settings.Default.Spy = true;
                                    break;
                            }
                            break;
                        case "hex":
                            switch (kv.Length)
                            {
                                case 2:
                                    Settings.Default.Hex = bool.Parse(kv[1]);
                                    break;
                                default:
                                    Settings.Default.Hex = true;
                                    break;
                            }
                            break;
                        case "hexformat":
                            Settings.Default.HexFormat = kv[1];
                            break;
                        case "sump2ols":
                            switch (kv.Length)
                            {
                                case 2:
                                    Settings.Default.Transparent = !bool.Parse(kv[1]);
                                    break;
                                default:
                                    Settings.Default.Transparent = false;
                                    break;
                            }
                            break;
                    }
                }
                catch (Exception e)
                {
                    LogErrorMessage(e);
                    LogStateMessage("Problems parsing configuration option: \"" + arg + "\". Ignoring and using default value...");
                }
            }
        }

        static void ShowSettings()
        {
            LogStateMessage("Current (persisted) configuration:");

            foreach (var prop in Settings.Default.GetType().GetProperties())
            {
                bool show = false;
                foreach (var attr in System.Attribute.GetCustomAttributes(prop))
                {
                    if (attr is System.Configuration.UserScopedSettingAttribute)
                    {
                        show = true;
                        break;
                    }
                }
                if (show)
                {
                    LogStateMessage("\t" + prop.Name + "=" + prop.GetValue(Settings.Default, null));
                }
            }
            LogStateMessage("");
        }

        static void SerialPortInitLoop()
        {
            bool err = true;

            do
            {
                try
                {
                    LogStateMessage("Getting Configured Serial Port Object from OS...");
                    serial_port = new SerialPort(
                        Settings.Default.SerialPortName,
                        Settings.Default.SerialBaud,
                        Settings.Default.Parity,
                        Settings.Default.DataBits,
                        Settings.Default.StopBits);
                    //Extra params
                    serial_port.DiscardNull = Settings.Default.DiscardNull;
                    serial_port.DtrEnable = Settings.Default.DtrEnable;
                    serial_port.Handshake = Settings.Default.Handshake;
                    serial_port.ParityReplace = Settings.Default.ParityReplace;
                    serial_port.ReadBufferSize = Settings.Default.ReadBufferSize;
                    serial_port.ReadTimeout = Settings.Default.ReadTimeOut;
                    serial_port.WriteBufferSize = Settings.Default.WriteBufferSize;
                    serial_port.RtsEnable = Settings.Default.RtsEnable;
                    serial_port.WriteTimeout = Settings.Default.WriteTimeOut;
                    err = false;
                    LogStateMessage("Done.");
                }
                catch (Exception e)
                {
                    err = true;

                    LogErrorMessage(e);

                    LogStateMessage("Could not get serial port " + Settings.Default.SerialPortName + "... waiting 5 seconds and retrying...");
                    Thread.Sleep(Settings.Default.RetryTimeOut);
                }
            } while (err && run);
        }

        static void TcpServerInitLoop()
        {
            bool err = true;
            do
            {
                try
                {
                    tcp_listener = new TcpListener(Settings.Default.TCPPort);
                    tcp_listener.Start();
                    err = false;
                }
                catch (Exception e)
                {
                    err = true;
                    LogErrorMessage(e);
                    LogStateMessage("Could not start listening for connections on port " + Settings.Default.TCPPort + "... waiting 5 seconds and retrying...");
                    Thread.Sleep(Settings.Default.RetryTimeOut);
                }
            } while (err && run);
        }

        static void SerialPortOpenLoop()
        {
            bool err = true;
            do
            {
                try
                {
                    serial_port.Open();
                    err = false;
                }
                catch (Exception e)
                {
                    err = true;
                    LogErrorMessage(e);
                    LogStateMessage("Error Opening serial port " + Settings.Default.SerialPortName + "... Retrying in 5 seconds...");
                    Thread.Sleep(Settings.Default.RetryTimeOut);
                }
            } while (err && run);
            LogStateMessage("Serial Port " + Settings.Default.SerialPortName + " Open. Relaying data asyncronously at " + Settings.Default.SerialBaud + "bps...");
        }

        static void CloseCommunications()
        {
            LogStateMessage("Closing...");
            try
            {
                serial_port.Close();
                tcp_client.Close();
            }
            catch (Exception e)
            {
                LogErrorMessage(e);
                LogStateMessage("Something went wrong while closing tcp/serial connections... Ignoring & continuing...");
            }
            LogStateMessage("Closed.");
        }

        static void InitiateAsyncCommunications()
        {
            keepAlive = true;

            tcp_client.ReceiveTimeout = Settings.Default.TCPRecieveTimeOut;
            tcp_client.SendTimeout = Settings.Default.TCPSendTimeOut;

            tcp_client.Client.BeginReceive(bufToSerial, 0, Settings.Default.InternalBufferLength,
                SocketFlags.None, AsyncReceiveFromPC, tcp_client);
            serial_port.BaseStream.BeginRead(bufFromSerial, 0, Settings.Default.InternalBufferLength,
                AsyncReceiveFromSerial, serial_port);
        }

        static void WaitWhileCommunicationsAreAlive()
        {
            while (keepAlive)
            {
                stayAliveAutoResetEvent.WaitOne();
            }
        }
        static void TcpClientAcceptAndHandleLoop()
        {
            while (run)
            {
                skip = 0;
                #region Accepting one client
                LogStateMessage("Waitting inbound tcp connection on port " + Settings.Default.TCPPort + "...");
                try
                {
                    tcp_client = tcp_listener.AcceptTcpClient();
                }
                catch (Exception e)
                {
                    LogErrorMessage(e);
                    LogStateMessage("Problems accepting connection... Please retry...");
                    continue;
                }
                LogStateMessage("Accepted TCP Client. Opening Serial...");
                #endregion

                SerialPortOpenLoop();

                #region Initializing Async Communications and Waiting to complete
                try
                {
                    InitiateAsyncCommunications();

                    WaitWhileCommunicationsAreAlive();
                }
                catch (Exception e)
                {
                    LogErrorMessage(e);
                    LogStateMessage("Something went wrong... Retrying everything in 5 seconds");
                    Thread.Sleep(Settings.Default.RetryTimeOut);
                    continue;
                }
                #endregion

                CloseCommunications();
            }
        }
        static void Main(string[] args)
        {
            //Improving debug info in case of unexpected failure
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            //Providing Ctrl+C means for shutting down console
            Console.CancelKeyPress += Console_CancelKeyPress;

            ParseParametersToSettings(args);

            ShowInfo();

            //Preparing Internal Send/Recieve buffers
            bufFromSerial = new byte[Settings.Default.InternalBufferLength];
            bufToSerial = new byte[Settings.Default.InternalBufferLength];

            ShowSettings();

            //Settings seem to be valid, attempting to persist them for easy (no param) restart
            Settings.Default.Save();

            //Updating console title to reflect config status
            Console.Title = "TCP2Serial " + version + " (tcp=" + Settings.Default.TCPPort
                + ", serial=" + Settings.Default.SerialPortName
                + ", baud=" + Settings.Default.SerialBaud
                + ", mode=" + (Settings.Default.Transparent ? "TRANSPARENT" : "SUMP2OLS ADAPTER") + ") ";


            SerialPortInitLoop();

            TcpServerInitLoop();

            TcpClientAcceptAndHandleLoop();
        }

        static bool firstCloseAttempt = true;
        static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            LogStateMessage("Attempting to stop everything and close... Wierd messages might show up :)");
            run = false;
            keepAlive = false;
            if (firstCloseAttempt)
            {
                try
                {
                    tcp_client.Close();
                }
                catch (Exception ex)
                {
                    LogErrorMessage(ex);
                }
                try
                {
                    serial_port.Close();
                }
                catch (Exception ex)
                {
                    LogErrorMessage(ex);
                }
                try
                {
                    tcp_listener.Stop();
                }
                catch (Exception ex)
                {
                    LogErrorMessage(ex);
                }

                e.Cancel = true;
            }
            firstCloseAttempt = false;
        }
        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            LogStateMessage("It seems something unforseen went wrong... Please analyze the exception trace: ");
            LogErrorMessage(new Exception(e.ExceptionObject.ToString()));
        }
    }
}

# "Insistent" TCP Server to Serial Port Adapter #
(v0.5 updated 2014-03-03)
*(with support for minor adaptation of [SUMP](http://www.sump.org/projects/analyzer/protocol/) Protocol for easier usage with [Open Logic Sniffer](http://www.lxtreme.nl/ols/) v9.7.0 Application)*

## About ##

Simple, console based, configurable TCP Socket adapter to Serial Port (COM port) with support for persistent settings across execution sessions useful for easy, parameter-less, restart of the application.

## How it Works? ##

When the application is started:

* previous saved application settings are being loaded
* any new command line parameters are parsed and used to update the persistent application settings
* a serial port object is being configured using the specified serial port settings
* a tcp server (listening port) is started and will be waiting for a new tcp client

When a client connects:

* the serial port is being opened
* the client is accepted
* data is being passed asynchronously and bidirectionally between the tcp and serial connections
* based on configured settings the data flow can be sniffed/viewed in the console
* the tcp server pauses waiting for a new client until the current tcp connection is being dropped

## Use Cases ##

* Make visible/usable a local Serial Port over the network
* Analyze serial port communication traffic
* Easy terminal console connection to a serial port connected device using **telnet**, **putty** or other tcp terminal based console software from local host or other network host
* OLS protocol support for aborting data acquisition

## Syntax ##
*(used to configure persistent settings across restarts)* 

tcp2serial

* [-help|-h|/help|/h|--help]
* [**serialport**=<name>] [**serialbaud**=<value_bps>]
* [**tcpport**=<listening_port>]
* [**transparent**[=true|=false]] [**sump2ols**[=true|=false]]
* [**databits**=<databits>] [**stopbits**=<None|One|OnePointFive|Two>]
* [**parity**=<None|Even|Odd|Mark|Space>] [**parityreplace**=<replacement_byte>]
* [**dtrenable**=<true|false>] [**rtsenable**=<true|false>]
* [**handshake**=<None|RequestToSend|RequestToSendXOnXOff|XOnXOff>]
* [**discardnull**=<true|false>]
* [**readbuffersize**=<buffer_size>] [**readtimeout**=<milliseconds>]
* [**writebuffersize**=<buffer_size>] [**writetimout**=<milliseconds>]
* [**tcprecievetimeout**=<milliseconds>] [**tcpsendtimeout**=<milliseconds>]
* [**retrytimeout**=<milliseconds>] [**internalbufferlength**=<buffer_size>]
* [**spy**[=true|=false]] [**hex**[=true|false]] [**hexformat**=<x2|X2>]
* [**silent**[=true|=false]] [**verbose**]

  **where:**

* <**description**> needs to be replaced accordingly;
* [**key** or **value collection**] denotes optional config key or value

*e.g. [=value1|=value2] the key can be configured explicitly with the specified value or left with default (positive) meaning of the key*

##  Parameter keys: ##
* **serialport**, **serialbaud**, **tcpport** -> essential & self explanatory
* **databits**, **stopbits**, **parity**, **handshake** -> extra serial options
* **discardnull**, **((read|write)buffer(size|timeout))** -> extra .net async operation options
* **tcp<send/recieve>timeout** -> socket async timing configuration
* **retrytimeout** -> time for the app to step back after an error
* **internalbufferlength** -> internal application send/reieve buffers sizes
* **transparent** -> transparent/direct data forwarding ("cable" replacement)
* **silent** -> inhibit all state messages
* **spy** -> log to console serial<->tcp communications
* **hex** -> use hex format for data logging
* **sump2ols** -> adapts the USART connection for easy Open Logic Sniffer usage
* **verbose** -> display detailed exceptions when encountering errors (option not persisted in configuration)
* **-h**, **-help**, **/h**, **/help** -> only show help and quit (option not persisted in configuration)
    
For the options not configured options there are considered previous configured values or default values. Malformed options are ignored.

## Example ##
tcp2serial serialport=COM1 serialbaud=115200 tcpport=8000 transparent

## Hex formatting ##

The hex format can be customized using the **hexformat** key. It accepts .Net [Standard Numeric Format Strings](http://msdn.microsoft.com/en-us/library/dwhawy9k%28v=vs.110%29.aspx) or  [Custom Numeric Format Strings](http://msdn.microsoft.com/en-us/library/0c899ak8%28v=vs.110%29.aspx).

This makes it possible to format the sent/received bytes not only in hex, but in other formats, e.g. formatting of byte 0x0A (hex) 10 (decimal):

* x -> a (hex, no capitalization)
* X -> A (hex, capitalized)
* X2 -> 0A (hex, capitalized, min 2 digits)
* x2 -> 0a (hex, no capitalization, min 2 digits)
* d -> 10 (decimal)
* d3 -> 010 (decimal, min 3 digits)